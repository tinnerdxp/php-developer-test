<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // register the id3 as a service
        $this->app->singleton('id3', function($app) {
            return new \getID3();
        });
    }
}
