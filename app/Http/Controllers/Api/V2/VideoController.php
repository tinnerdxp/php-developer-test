<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Laravel\Lumen\Application;

class VideoController extends Controller {

    protected $basePath;
    protected $getId3Analyzer;

    /**
     * VideoController constructor.
     * @param Application $app
     * @param \getID3 $getId3Analyzer
     */
    public function __construct(Application $app, \getID3 $getId3Analyzer)
    {
        $this->basePath = $app->basePath();

        // get the library as a service and store locally
        $this->getId3Analyzer = $getId3Analyzer;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function upload(Request $request): JsonResponse
    {
        try
        {
            // validate we have all we need
            $this->validateRequest($request);

            // once the validation passes - move the file to target location
            $targetTemporaryVideoPath = $this->processUploadedFileToTargetLocation($request);

            // extract the ID3 metadata
            $metaData = $this->getVideoMetadata($targetTemporaryVideoPath);

        }
        catch (\Exception $e)
        {
            return $this->returnErrorResponse($e->getMessage(), 400);
        }

        // response
        return $this->returnSuccessfulResponse($metaData);
    }

    /**
     * Validates if the request contains the right payload
     * Uses a list of validators that can be easily extended
     *
     * @param Request $request
     * @return string
     */
    private function validateRequest(Request $request)
    {
        $maxFileSize = ini_get('upload_max_filesize'); // if this is exceeded we get $_FILES['error'] = 1;
        $postMaxSize = ini_get('post_max_size');       // if this is exceeded we get $_FILES = [];

        /* suggested settings:
            max_file_uploads=100M
            upload_max_filesize=100M
            post_max_size=110M
            max_execution_time=240
         */

        $file = $request->file('data');
        if ($file === null)
        {
            throw new \InvalidArgumentException('File is either larger than '. $postMaxSize . ' or you haven\'t uploaded it');
        }
        else
        {
            switch($file->getError())
            {
                case UPLOAD_ERR_OK:
                    return true;
                case UPLOAD_ERR_INI_SIZE:
                    throw new \InvalidArgumentException('The uploaded file exceeds the upload_max_filesize directive in php.ini which is currently set to: ' . $maxFileSize);
                case UPLOAD_ERR_FORM_SIZE:
                    throw new \InvalidArgumentException('The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form');
                case UPLOAD_ERR_PARTIAL:
                    throw new \InvalidArgumentException('The uploaded file was only partially uploaded');
                case UPLOAD_ERR_NO_FILE:
                    throw new \InvalidArgumentException('No file was uploaded');
                case UPLOAD_ERR_NO_TMP_DIR:
                    throw new \InvalidArgumentException('Missing a temporary folder');
                case UPLOAD_ERR_CANT_WRITE:
                    throw new \InvalidArgumentException('Failed to write file to disk');
                default:
                    throw new \InvalidArgumentException('There was some other upload error');
            }
        }
    }

    /**
     * @param string $err
     * @param int $httpErrorCode
     * @return \Illuminate\Http\JsonResponse
     */
    private function returnErrorResponse(string $err, int $httpErrorCode): JsonResponse
    {
        return response()->json(['result' => 'error', 'error' => $err], $httpErrorCode);
    }

    /**
     * @param array $payload
     * @return \Illuminate\Http\JsonResponse
     */
    private function returnSuccessfulResponse(array $payload): JsonResponse
    {
        return response()->json(['result' => 'success', 'meta' => $payload], 200);
    }

    /**
     * This would obviously be based off some config and potentially use different strategy, "cloud" location using NFS,
     * upload to S3 or most likely queued processing
     * @param Request $request
     */
    private function processUploadedFileToTargetLocation(Request $request): string {
        $targetLocation = $this->basePath . env('VIDEO_TARGET_LOCATION');

        // the naming convention should probably store a hash per user and then manage the folders properly. Here we
        // will just use the filename for simplicity
        $file = $request->file('data');
        $file->move($targetLocation, $file->getClientOriginalName());
        return $targetLocation . '/' . $file->getClientOriginalName();
    }

    /**
     * Obtains metadata for given file
     * @param string $path
     * @return array
     */
    private function getVideoMetadata(string $path): array {
        $metadata = $this->getId3Analyzer->analyze($path);

        // I've started to get UTF8 malformed errors on
        // $payload['quicktime']['moov']['subatoms'][2]['subatoms'][2]['subatoms'][2]['subatoms'][2]['subatoms'][0]
        //['sample_description_table'][0]['compressor_name'],
        // I'm guessing it's UTF16 or 32 triggering the json_encode to fail, but for the purpose of this test I will
        // simply remove the whole quicktime section
        unset($metadata['quicktime']);

        return $metadata;
    }
}