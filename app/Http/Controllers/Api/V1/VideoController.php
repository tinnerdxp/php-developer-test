<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;

class VideoController extends Controller {
    public function upload() {
        return response()->json(['result' => 'error', 'error' => 'API Version 1 is deprecated, please upgrade to v2'], 400);
    }
}