<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Http\UploadedFile;

class Version2Test extends TestCase
{
    public function testIfGetIsReturns405()
    {
        $this->get('/api/v2/upload');
        $this->assertEquals(405, $this->response->getStatusCode());
    }

    public function testIfNoFileReturnsError() {
        $this->post('/api/v2/upload');
        $this->assertEquals(400, $this->response->getStatusCode());
        $this->assertContains('File is either larger than', $this->response->getContent());
    }

    public function testIfUploadingWorks() {
        // we need to make a backup of the test video as it will be moved
        $filename = 'test_' . str_replace('.', '', microtime(true)) . '.mp4';
        copy(app()->basePath() . '/tests/test_videos/example.mp4', sys_get_temp_dir() . '/' . $filename);
        $file = new UploadedFile(sys_get_temp_dir() . '/' . $filename, $filename, null, null, null, true);

        // issue request
        $this->call('POST', '/api/v2/upload', [], [], ['data' => $file]);

        // asset
        $payload = json_decode($this->response->getContent(), true);
        $this->assertEquals('mp4',       $payload['meta']['fileformat']);
        $this->assertEquals('4972761',   $payload['meta']['filesize']);
        $this->assertEquals('video/mp4', $payload['meta']['mime_type']);
        $this->assertEquals('0:44',      $payload['meta']['playtime_string']);
        $this->assertEquals(200, $this->response->getStatusCode());
        $this->assertTrue(file_exists(app()->basePath() . env('VIDEO_TARGET_LOCATION') . '/' . $filename));

        // clear up
        unlink(app()->basePath() . env('VIDEO_TARGET_LOCATION') . '/' . $filename);
    }
}
