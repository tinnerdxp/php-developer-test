<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class Version1IsDeprecatedTest extends TestCase
{
    public function testIfVersionOneIsDeprecated()
    {
        $this->post('/api/v1/upload');

        $this->assertEquals(
            '{"result":"error","error":"API Version 1 is deprecated, please upgrade to v2"}',
            $this->response->getContent()
        );
    }
}
