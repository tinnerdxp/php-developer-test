<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// default controller
$router->get('/', ['uses' => 'DefaultController@index']);

// v1 - deprecated
$router->group(
    [
        'prefix' => '/api/v1',
        'namespace' => '\App\Http\Controllers\Api\V1'
    ],
    function($app)
    {
        $app->post('upload', 'VideoController@upload');
    }
);

// v2 - active
$router->group(
    [
        'prefix' => '/api/v2',
        'namespace' => '\App\Http\Controllers\Api\V2'
    ],
    function($app)
    {
        $app->post('upload', 'VideoController@upload');
    }
);

